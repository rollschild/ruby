class Ticket
  attr_reader :venue, :date, :price
  attr_writer :price
  attr_accessor :number_of_people

  def initialize(venue, date)
    @venue = venue
    @date = date
  end
end

ticket = Ticket.new('My Apartment', '01/30/2019')
ticket.price = 20.37
ticket.number_of_people = 12

puts "The event is at #{ticket.venue}, on #{ticket.date}."
puts "Price is #{ticket.price}."
puts "#{ticket.number_of_people} people will come over to the event."
