require_relative 'stacklike'

class Stack
  include Stacklike
end

stack = Stack.new
stack.add('item one')
stack.add('item two')
stack.add(-1992)
stack.add(['hello', 2037])

print stack.stack
print "\n"
print stack.stack.inspect, "\n"
# ["item one", "item two", -1992, ["hello", 2037]]

puts stack.stack
=begin
item one
item two
-1992
hello
2037
=end

stack.remove
print stack.stack, "\n"
stack.remove
print stack.stack, "\n"
