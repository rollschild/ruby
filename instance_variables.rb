class Person
  def initialize
    puts 'Initializing the Person object...'
  end

  def name=(string)
    puts 'Setting a person\'s name...'
    @name = string
    puts 'Invisible...'
  end

  def name_reader
    puts 'Retrieving this person\'s name...'
    @name
  end
end

jovi = Person.new

# syntatic sugar
print 'Return value of the method is '
puts jovi.name = 'Jovi'

puts jovi.name_reader

class Ticket
  def initialize(venue, date)
    @venue = venue
    @date = date
  end

  def venue_reader
    @venue
  end

  def date_reader
    @date
  end
end

th = Ticket.new('Town Hall', '01/29/2019')
cc = Ticket.new('Convention Center', '10/21/2018')

puts 'Two tickets werer created.'
puts "First ticket is at #{th.venue_reader}, on #{th.date_reader}."
puts "Second ticket is at #{cc.venue_reader}, on #{cc.date_reader}."
