def list_args(*args)
  p args
end

list_args(12, 3, 'stringggg', true)

def mixed_args(arg_one, arg_two, *args, arg_final)
  puts 'Arguments: '
  p arg_one, arg_two, args, arg_final
end

mixed_args(1, 2, 3)
