obj = Object.new

def obj.talk
  puts 'I\'m an object'
  puts 'what\'s your identity?'
end

obj.talk

def obj.c2f(celsius)
  celsius * 9.0 / 5 + 32
end

puts obj.c2f(100)
