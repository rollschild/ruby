ticket = Object.new

def ticket.date
  current_time = Time.now
  year = current_time.year.to_s
  month = current_time.month.to_s
  day = current_time.day.to_s

  return year + '/' + month + '/' + day
end

def ticket.venue
  'Town Hall'
end

def ticket.event
  "Author's reading.."
end

def ticket.performer
  'Jonathan Franzen'
end

def ticket.seat
  'Front row, in the middle'
end

def ticket.price
  20.3701
end

=begin
print 'Ticket is for ' + ticket.event + ', at ' + ticket.venue
puts ', on ' + ticket.date + '.'
puts 'Performer is ' + ticket.performer + '.'
print 'Your seat is at ' + ticket.seat + ','
print ' and it costs $'
puts format('%.2f.', ticket.price)
=end

puts "Ticket is for: #{ticket.event}, at #{ticket.venue}. " \
  "Performer is #{ticket.performer}. " \
  "Your seat is at #{ticket.seat}, " \
  "and it costs you $#{format('%.2f.', ticket.price)}"

print 'Enter what you want: '
request = gets.chomp

if ticket.respond_to?(request)
  puts ticket.__send__(request)
else
  puts 'No such information available!'
end
