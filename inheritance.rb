class Publication
  attr_accessor :publisher
end

class Magazine < Publication
  attr_accessor :editor
end

mag = Magazine.new
mag.publisher = 'Guangchu Shi'
mag.editor = '...also Guangchu Shi'

def Magazine.name 
  'Wired'
end

puts Magazine.name
puts mag.respond_to?('name')

print "Magazine is published by #{mag.publisher}, "
puts "and its editor is #{mag.editor}."
puts Magazine.superclass
puts Magazine.superclass.superclass
puts Magazine.superclass.superclass.superclass
puts Magazine.superclass.superclass.superclass.superclass
