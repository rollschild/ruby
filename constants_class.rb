class Ticket
  VENUES = ['Convention Center', 'Midland', 'Town Hall'] #.freeze
  attr_reader :venue, :date

  def initialize(venue, date)
    # guard clause
    raise ArgumentError, "Unkown venue #{venue}!" unless VENUES.include?(venue)

    @venue = venue
    @date = date
  end
=begin
  def initialize(venue, date)
    if VENUES.include?(venue)
      @venue = venue
    else
      raise ArgumentError, "Unkown venue #{venue}!"
    end

    @date = date
  end
=end
end

ticket = Ticket.new('Midland', '02/07/2019')
puts ticket.venue
puts Ticket::VENUES
Ticket::VENUES << "Gym"
puts Ticket::VENUES
Ticket::VENUES = ["NEW WOO HAHAHA"]
puts Ticket::VENUES
