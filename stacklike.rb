module Stacklike
  def stack
    @stack ||= []
  end

  def add(obj)
    stack.push(obj)
  end

  def remove
    stack.pop # also returns the obj
  end
end
