def modify_string(str)
  str.replace('It is a new string...')
end

original_str = 'Original content.'

puts original_str
modify_string(original_str)
puts original_str

will_not_change_str = 'Original content'

puts will_not_change_str
# clone
# freeze
modify_string(will_not_change_str.dup)
puts will_not_change_str
