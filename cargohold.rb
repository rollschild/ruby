require_relative 'stacklike'

class Suitcase
end

class CargoHold
  include Stacklike

  def load_then_report(obj)
    print 'Loading cargo: '
    puts obj.object_id

    add(obj)
  end

  def unload
    remove
  end
end

cargo = CargoHold.new
suit_one = Suitcase.new
suit_two = Suitcase.new
suit_three = Suitcase.new

cargo.load_then_report(suit_one)
cargo.load_then_report(suit_two)
cargo.load_then_report(suit_three)

first_unloaded = cargo.unload
print 'First unloaded luggage is... '
puts first_unloaded.object_id
